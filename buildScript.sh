#!/bin/sh
if [ -z ${TARGET} ]; then
	echo "Environment variable TARGET must be defined."
	exit 12
fi

if [ -z ${WORKSPACE} ]; then
	echo "Environment variable WORKSPACE must be defined."
fi

if [ -z ${APP_NAME} ]; then
	export APP_NAME=${TARGET}
	echo "APP_NAME is not defined, usiong ${APP_NAME}"
fi

if [ -z ${PROJECT_ROOT} ]; then
	export PROJECT_ROOT=${WORKSPACE}
	echo "PROJECT_ROOT is not defined, using ${PROJECT_ROOT}"
fi					

	
if [ -z ${SCHEME} ]; then
	echo "SCHEME is not defined, using ${TARGET}"
	export SCHEME=${TARGET}
fi
if [ -z ${XCODE_WORKSPACE} ]; then
	export XCODE_WORKSPACE=${TARGET}.xcworkspace
	echo "XCODE_WORKSPACE is not defined, using ${XCODE_WORKSPACE}"	
fi

if [ -z ${XCODE_PROJECT} ]; then
	export XCODE_PROJECT=${TARGET}.xcodeproj
	echo "XCODE_PROJECT is not defined, using ${XCODE_PROJECT}"
fi


if [ -z ${BUILD_ROOT} ]; then
	export BUILD_ROOT=${WORKSPACE}/build
	echo "BUILD_ROOT is not defined, using ${BUILD_ROOT}"
fi

if [ -z ${SYMROOT} ]; then
	export SYMROOT=${BUILD_ROOT}/sym.root
	echo "SYMROOT is not defined, using ${SYMROOT}"
fi

if [ -z ${OBJROOT} ]; then
	export OBJROOT=${BUILD_ROOT}/obj.root
	echo "OBJROOT is not defined, using ${OBJROOT}"
fi	
if [ -z ${DSTROOT} ]; then
	export DSTROOT=${BUILD_ROOT}/dst.root
	echo "DSTROOT is not defined, using ${DSTROOT}"
fi	

if [ -e "${PROJECT_ROOT}/${XCODE_WORKSPACE}" ]; then
	export BUILD_COMMAND_ROOT="-scheme ${SCHEME} -workspace ${XCODE_WORKSPACE}"
else
	export BUILD_COMMAND_ROOT="-target ${TARGET} -project ${XCODE_PROJECT}"
fi

if [ -z ${CONFIGURATION} ]; then
	export CONFIGURATION=Release
	echo "CONFIGURATION is not defined, using ${CONFIGURATION}"
fi

cd ${PROJECT_ROOT} 
#rm -rf ${BUILD_ROOT}

xcodebuild ${BUILD_COMMAND_ROOT} OBJROOT=${OBJROOT} SYMROOT=${SYMROOT} DSTROOT=${DSTROOT} -configuration ${CONFIGURATION} build
export buildResult=$?
if [ $buildResult -ne 0 ]; then
	exit $buildResult
fi

if [ -e "${SYMROOT}/${CONFIGURATION}-iphoneos/" ]; then
	cd ${SYMROOT}/${CONFIGURATION}-iphoneos
	if [ -e Payload ]; then
		rm -rf Payload
	fi
	if [ -e ${APP_NAME}.ipa ]; then
		rm -f ${APP_NAME}.ipa
	fi
	
	if [ -e ${APP_NAME}.app ]; then
		mkdir Payload
		mv ${APP_NAME}.app Payload/
		zip -r ${APP_NAME}.ipa Payload
	else
		echo "File not found `pwd`/${APP_NAME}.app"
		exit 12
	fi 
fi

